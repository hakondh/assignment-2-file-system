# File system

## About

Java application that can be used to manage and manipulate files. Every action is logged to a file called log.txt, which is to be found in logs/. Possible errors that could occur from invalid user input or if something goes wrong during file manipulation, are handeled. <br>

The application uses

- File
- Scanner
- FileFilter
- FileReader
- FileWriter
- BufferedReader
- FileInputStream

The program also uses some simple regex for certain functions. 

## How to run

The program should be exectued using the tools of an IDE, as running the .jar file will cause an error due to the relative paths to the resources folder. To do this, navigate to the entry point of the program, src/main/java/FileSystemProgram.java, and run the program from there. 

The .jar file in out/ *can* be run using `java -jar FileSystemProgram.jar`, but every command will fail to do anything. 


