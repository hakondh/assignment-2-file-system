package main.java;

import java.util.InputMismatchException;
import java.util.Scanner;

import main.java.filemanipulation.DraculaFileInfo;
import main.java.filemanipulation.FileLister;

/* This class handles all communcation with the user. This means viewing menus, recieving user input, 
sending the data to be processed and displaying results or errors.  */

class UserInteraction {
    private final Scanner scanner = new Scanner(System.in);
    private boolean run = true;

    public void run() {
        System.out.println("Welcome to the file system");
        // Loop that will run as long as the user wants to interact with the file system
        while(run) {
            showMainMenu();
        }
        scanner.close();
        System.out.println("Bye!");
    }

    private void showMainMenu(){
        int choice;
        String[] options = {"List all files", "List files by extension", "Manipulate Dracula.txt", "Quit"};
        choice = getUserInput("Select an option", options);

            // Run approperiate method depending on user input
        switch(choice) {
            case 1 -> printAllFiles();
            case 2 -> printFilesByExtension();
            case 3 -> showDraculaMenu();
            case 4 -> this.run = false;
        }
    }

    // Shows the "Dracula menu"
    private void showDraculaMenu() {
        String[] options = {
                "Get the name of the file.",
                "Show size of the file",
                "Show how many lines it has",
                "Search for a word",
                "Get the number of occurrences of a word",
                "Go back"
        };
        int choice = getUserInput("What do you want to do?", options);

        String output = "";
        long startTime = 0;
        switch (choice) {
            case 1 -> {
                startTime = System.currentTimeMillis();
                output = DraculaFileInfo.getFileName() + ". ";
            }
            case 2 -> {
                startTime = System.currentTimeMillis();
                double size = DraculaFileInfo.getFileSize();
                if (size != -1.0) output = (size + " kB. ");
            }
            case 3 -> {
                startTime = System.currentTimeMillis();
                int lineCount = DraculaFileInfo.getLineCount();
                if (lineCount != -1) output = "Counted " + lineCount + " lines. ";
            }
            case 4 -> {
                System.out.println("Please enter the word you want to search for");
                scanner.nextLine(); // Consume the line break
                String word = scanner.nextLine();
                startTime = System.currentTimeMillis();
                if (DraculaFileInfo.wordIsInText(word)) output = "The word " + word + " was found. ";
                else output = "Could not find the word " + word + ". ";
            }
            case 5 -> {
                System.out.println("Please enter the word you want to count");
                scanner.nextLine(); // Consume the line break
                String countWord = scanner.nextLine();
                String lowerCaseCountWord = countWord.toLowerCase();
                startTime = System.currentTimeMillis();
                int count = DraculaFileInfo.getWordCount(lowerCaseCountWord);
                if (count != -1) output = "The word " + countWord + " was found " + count + " times. ";
                else output = "Could not find a single occurrence of the word " + countWord + ". ";
            }
        }
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        // Only log if we got results
        if(!output.isEmpty()) {
            System.out.println(output);
            Logger.log(output, time);
        }
    }
 
    // Gets file names and prints it out
    private void printAllFiles() {
        long startTime = System.currentTimeMillis();
        String[] fileNames = FileLister.getFileNames();
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        if(fileNames == null) return; // Null as return means something went wrong
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < fileNames.length; i++) {
            output.append(fileNames[i]);
            output.append(i == fileNames.length - 1 ? ". " : ", "); // Format the output list of files
        } 
        System.out.println(output);
        Logger.log(output.toString(), time);
    }

    // Gets file names by extension and prints it out
    private void printFilesByExtension() {
        String[] options = {".txt", ".jpeg", ".png", ".jpg", ".jfif", "Go back"}; // The options
        int choice = getUserInput("Please choose an extension", options);

        if(choice == options.length) return; // The last option is go back

        long startTime = System.currentTimeMillis();
        String[] fileNames = FileLister.getFileNames(options[choice - 1]);
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        if(fileNames == null) return; // Null as return means something went wrong
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < fileNames.length; i++) {
            output.append(fileNames[i]);
            output.append(i == fileNames.length - 1 ? ". " : ", "); // Format the output list of files
        }
        System.out.println(output);
        Logger.log(output.toString(), time);
    }

    // Takes a title and options, displays them, and returns what the user answers
    private int getUserInput(String  title, String[] options) {
        // This loop will end (with the return statement) if the user enters a valid value
        while(true) {
            try {
                System.out.println("\n" + title);
                for(int i = 0; i < options.length; i++)
                    System.out.println("(" + (i + 1) + ") " + options[i]);
                int choice = scanner.nextInt();
                if(choice > 0 && choice < options.length + 1 ){
                    return choice;
                }
                System.out.println("Please select value in range 1-" + options.length + "\n");
            }
            catch(InputMismatchException ex) {
                scanner.next(); // Clear the wrong input
                System.out.println("Invalid input");
            }
        }

    }
}