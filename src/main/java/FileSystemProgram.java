package main.java;

/* Entry point for the program */

class FileSystemProgram {
    public static void main(String[] args){
        new UserInteraction().run();
    }
}
