package main.java.filemanipulation;

import java.io.File;
import java.io.FileFilter;

/* This class handles manipulation that is related to the resources as a whole */

public class FileLister {
    /* Methods "getFileNames" are overloaded. Providing an extension
    as an argument is optional.  */

    public static String[] getFileNames() {
         return getFileNames(null);
    }

    public static String[] getFileNames(String extension) {
        try {
            // Find the folder, and get the files contained by it
            File folder = new File("src/main/resources"); 
            FileFilter filter = null;
            // If extension is provided, we create a FileFilter
            if(extension != null) {
                filter = new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().toLowerCase().endsWith(extension);
                    }
                };
            }
            
            // Get the files, possibly filtered
            File[] files = folder.listFiles(filter);        
            String [] fileNames = new String[files.length]; 

            // Convert the files to string, and put them in an array
            for(int i = 0; i < files.length; i++) fileNames[i] = files[i].getName();
            return fileNames;
        } 
        // This exception is thrown if any security manager denies access to the folder
        catch(SecurityException ex) {
            System.out.println("Access denied!");
            System.out.println(ex.getMessage());
        }
        catch(NullPointerException ex) {
            System.out.println("Provide path to file");
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
