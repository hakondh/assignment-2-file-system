package main.java.filemanipulation;

import java.io.*;
import java.util.Scanner;
import java.util.regex.Pattern;

/* This class performs all the manipulation related to the Dracula.txt file  */

public class DraculaFileInfo {
    private static final String CONSTANT_PATH_TO_FILE = "src/main/resources/Dracula.txt";

    // Finds the file and returns the name
    public static String getFileName() {
        try  {
            return new File(CONSTANT_PATH_TO_FILE).getName();
        }
        // The only Exception a new File can throw
        catch(NullPointerException ex) {
            System.out.println("Provide a file path");
            System.out.println(ex.getMessage());
        }
        return null;
    }

    /* Creates a FileInputStream, iterates through the data, and returns the size
    Using File.length is significantly faster, but FileInputStream is used here to test it out */
    public static double getFileSize() {
        System.out.println("Getting file size...");
        // Resource is closed at the end of the statement (try-with-resources statement)
        try (FileInputStream fileInputStream = new FileInputStream(CONSTANT_PATH_TO_FILE)) {
            int data = fileInputStream.read();
            int byteCount = 0;

            // When we get -1, there is no more data
            while(data != -1) {
                byteCount++;
                data = fileInputStream.read();
            }

            // Return the size in kB
            return byteCount / 1024.0;
        }
        catch(IOException ex) {
            System.out.println("Something went wrong when trying to find the file size");
            System.out.println(ex.getMessage());
        }
        catch(SecurityException ex) {
            System.out.println("Access denied to file");
            System.out.println(ex.getMessage());
        }
        return -1.0; // This signifies an error
    }

    // Creates a BufferedReader and counts lines of the document
    public static int getLineCount() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(CONSTANT_PATH_TO_FILE))) {
            int count = 0;
            while ((bufferedReader.readLine()) != null) count++;
            return count;
        }
        catch(IOException ex) {
            System.out.println("Something went wrong reading the file");
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    // Creates a Scanner, and returns true if the word is found in the text
    public static boolean wordIsInText(String searchWord) {
        try(Scanner scanner = new Scanner(new File(CONSTANT_PATH_TO_FILE))){
            // Create a pattern to use in delimiter. Spaces, newlines and punctuations will separate the words
            Pattern pattern = Pattern.compile(" |\\n|\\p{Punct}");
            scanner.useDelimiter(pattern);

            String word;
            while(scanner.hasNext()){
                word = scanner.next();
                // Check if the word, in lower case, matches the search word
                if(word.equalsIgnoreCase(searchWord)) return true;
            }

            return false;
        }
        catch (IOException ex) {
            System.out.println("Something went wrong reading the file");
            System.out.println(ex.getMessage());
        }
        catch(NullPointerException ex) {
            System.out.println("Provide a path to the file");
            System.out.println(ex.getMessage());
        }
        return false;
    }

    // Creates a Scanner and counts the number of occurrences of the given search word
    public static int getWordCount(String searchWord) {
        try(Scanner scanner = new Scanner(new File(CONSTANT_PATH_TO_FILE))){
            // How we seperate words from each other
            Pattern pattern = Pattern.compile(" |\\n|\\p{Punct}");
            scanner.useDelimiter(pattern);

            String word;
            
            int count = 0;
            while(scanner.hasNext()){
                word = scanner.next();
                if(word.equalsIgnoreCase(searchWord)) count++;
            }

            return count;
        }
        catch (IOException ex) {
            System.out.println("Something went wrong reading the file");
            System.out.println(ex.getMessage());
        }
        catch(NullPointerException ex) {
            System.out.println("Provide a path to the file");
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
