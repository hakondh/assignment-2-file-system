package main.java;

import java.io.FileWriter;
import java.io.IOException;
import java.time.*; 

/* This class handles logging to log.txt */

public class Logger {
    
    // Takes output and function execution time, and writes it to a log file
    public static void log(String output, long time) {
        try (FileWriter filewriter = new FileWriter("logs/log.txt", true)){
            LocalDateTime localDateTime = LocalDateTime.now();
            filewriter.write(localDateTime + ": " + output + "The function took " + time + " ms to execute. \n");
          } catch (IOException ex) {
            System.out.println("Something went wrong trying to write to log");
            System.out.println(ex.getMessage());
          }
    }
}
